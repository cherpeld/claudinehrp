/** Class representant la liste des restaurants */
class ListRestaurants {
    constructor() {
        this._listRestaurant = [];
        this._nextId = 1;
        this.filter = ""
    }

    /**
     **cleanRestaurants
     *
     * Methode pour effacer la liste restaurants quand on se déplace sur la carte
     *
     *
     */
    cleanRestaurants(){
        this._listRestaurant = []
    }

    /**
     **listRestaurant
     *
     * Methode qui retourne les restaurant et s'il y a un filtre, la methode retourne les restaurants filtrés
     *
     */
    get listRestaurant() {
        return this.filterRestaurant(this.filter)
    }

    /**
     **addRestaurant
     *
     * Methode pour l'ajout d'un nouveau restaurant
     *
     * @param newRestaurant
     *
     */
    addRestaurant(newRestaurant) {
        newRestaurant.id = this._nextId;
        this._nextId++;
        this._listRestaurant.push(newRestaurant)
    }

    /**
     **filterRestaurant
     *
     * Methode pour le filtre des notes des restaurants
     *
     * @param filter
     *
     */
    filterRestaurant(filter) {

        this.filter = filter;
        let restaurants = [];

        for (let i = 0; i < this._listRestaurant.length; i++) {
            const restaurant = this._listRestaurant[i];
            let averages = restaurant.getAverage();
            switch (filter) {
                case 'onetotow':
                    if (averages >= 1 && averages <= 2) {
                        restaurants.push(restaurant);
                    }
                    break;
                case 'towtothree':
                    if (averages >= 2 && averages <= 3) {
                        restaurants.push(restaurant);
                    }
                    break;
                case 'threetofour':
                    if (averages >= 3 && averages <= 4) {
                        restaurants.push(restaurant);
                    }
                    break;
                case 'fourtofive':
                    if (averages >= 4 && averages <= 5) {
                        restaurants.push(restaurant);
                    }
                    break;
                default:
                    restaurants.push(restaurant);
            }
        }
        return restaurants
    }
}

//Création liste restaurant
const listRestaurantClass = new ListRestaurants();